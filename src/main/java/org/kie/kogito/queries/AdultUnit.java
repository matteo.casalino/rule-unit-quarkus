package org.kie.kogito.queries;

import java.io.Serializable;

import org.kie.kogito.conf.SessionsPool;
import org.kie.kogito.rules.DataSource;
import org.kie.kogito.rules.DataStore;
import org.kie.kogito.rules.RuleUnitData;

@SessionsPool(1)
public class AdultUnit implements RuleUnitData, Serializable {
    private int adultAge;

    private DataStore<Person> persons;

    public AdultUnit( ) {
        this( DataSource.createStore() );
    }

    public AdultUnit( DataStore<Person> persons ) {
        this.persons = persons;
    }

    public DataStore<Person> getPersons() {
        return persons;
    }

    public void setPersons( DataStore<Person> persons ) {
        this.persons = persons;
    }

    public int getAdultAge() {
        return adultAge;
    }

    public void setAdultAge( int adultAge ) {
        this.adultAge = adultAge;
    }
}